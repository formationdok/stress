# stress

Ajuster la commande `CMD stress -c 10` avec le nombre de coeurs de la machine.

Utiliser la commande Linux `htop` pour voir l'utilisation des coeurs (l'installer si besoin).
